using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public float x;
	public float y;
	private bool x1 = true;
	private bool y1 = true;


	public float speed;
	void Awake(){
		if (y == 0) {
			y = transform.position.y;
			y1 = false;
		}
		if (x == 0) {
			x = transform.position.x;
			x1 = false;
		}
	}
	void FixedUpdate() {
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, new Vector3(x,y,transform.position.z), step);

		if (x1 && transform.position.x == x) {//&& transform.position.y == y
			x = -x;
			//y = -y;
		} else if (y1 && transform.position.y == y) {
			y = -y;
		} else if(y1 && x1 && (transform.position.x == x || transform.position.y == y)){
			x = -x;
			y = -y;
		}
	}

}
