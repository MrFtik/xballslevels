﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour {
	public GameObject gameOverPanel;
	public GameObject other;

	void OnTriggerEnter2D(Collider2D other){
		Destroy (other.gameObject);
		gameOverPanel.SetActive (true);
	}
}
